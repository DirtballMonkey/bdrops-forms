<?php

declare(strict_types=1);

namespace Bdrops\Forms\Listener;

use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\ListenerInterface;
use Bdrops\CQRS\Services\CommandBus;

class FormCloneListener extends FormBaseListener implements ListenerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(CommandBus $commandBus, EventInterface $event): void
    {
        // Update the FormRead Model.
        $formUuid = $event->getCommand()->getAggregateUuid();
        $this->formService->updateFormRead($formUuid);
    }
}

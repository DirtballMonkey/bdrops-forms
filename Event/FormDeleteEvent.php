<?php

declare(strict_types=1);

namespace Bdrops\Forms\Event;

use Bdrops\Forms\Command\FormDeleteCommand;
use Bdrops\Forms\Listener\FormDeleteListener;
use Bdrops\CQRS\Event\Event;
use Bdrops\CQRS\Interfaces\EventInterface;

class FormDeleteEvent extends Event implements EventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return FormDeleteCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getListenerClass(): string
    {
        return FormDeleteListener::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return 'Form deleted';
    }

    /**
     * {@inheritdoc}
     */
    public static function getCode(): int
    {
        return CODE_OK;
    }
}

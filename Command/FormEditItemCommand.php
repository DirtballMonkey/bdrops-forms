<?php

declare(strict_types=1);

namespace Bdrops\Forms\Command;

use Bdrops\Forms\Handler\FormEditItemHandler;
use Bdrops\Forms\Model\Form;
use Bdrops\CQRS\Command\Command;
use Bdrops\CQRS\Interfaces\CommandInterface;

class FormEditItemCommand extends Command implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getHandlerClass(): string
    {
        return FormEditItemHandler::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getAggregateClass(): string
    {
        return Form::class;
    }
}

<?php

declare(strict_types=1);

namespace Bdrops\Forms\Command;

use Bdrops\Forms\Handler\FormShiftItemHandler;
use Bdrops\Forms\Model\Form;
use Bdrops\CQRS\Command\Command;
use Bdrops\CQRS\Interfaces\CommandInterface;

class FormShiftItemCommand extends Command implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getHandlerClass(): string
    {
        return FormShiftItemHandler::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getAggregateClass(): string
    {
        return Form::class;
    }
}
